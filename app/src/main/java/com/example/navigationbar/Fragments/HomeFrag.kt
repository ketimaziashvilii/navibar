package com.example.navigationbar.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.navigationbar.R

class HomeFrag : Fragment(R.layout.frag_home) {

    private lateinit var editTextAmount: EditText
    private lateinit var buttonSend: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = Navigation.findNavController(view)

        buttonSend.setOnClickListener{

            val amountText = editTextAmount.text.toString()

            if (amountText.isEmpty()) {
                return@setOnClickListener

            }

            val amount = amountText.toInt()
            val action = HomeFragDirections.actionHomeFrag2ToFragDash(amount)

            controller.navigate(action)






        }
    }
}